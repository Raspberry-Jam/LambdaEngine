# LambdaEngine

An experimental game engine written in the Odin programming language, currently using OpenGL. Loosely based off of existing code from the discontinued Exodus engine. It is intended to be a learning experience, where I will experiment with how to best implement the individual components of a high performance game engine. Don't expect much, I have almost no experience in this.

## TODO
These are tasks that need to be completed before moving on to further planned features.
- Rework project structure to be a statically linked library
- Implement a sandbox testing layer for the library
- Rework the windowing and input API to be a little more developer-friendly and require less passing of pointers

## Planned Features
Once the pending tasks are completed, these will be broken down into smaller tasks and taken on one at a time.
- Mesh loading
- Abstract graphics pipeline state machine
- Asset system (design needs heavy consideration, idk what I'm doing for this one yet)
- Events queue
- Program layer stack (events, rendering, etc handling in each layer)

## Potential Future Features
- Asynchronous multithreading
- Entity Component System
- Node-based render material definitions
- Multiplayer API
- Vulkan/Metal Renderer Back-ends