package window

import "core:log"
import "core:fmt"
import "core:strings"

import gl "vendor:OpenGL"
import "vendor:glfw"

import "lambda:global"

@(private)
lambda_windows := make(map[glfw.WindowHandle]^Lambda_Window)

@(private)
framebuffer_size_callback :: proc(window: glfw.WindowHandle, width: i32, height: i32) {
    gl.Viewport(0, 0, width, height)
    if lw, ok := lambda_windows[window]; ok {
        for listener in lw.resize_callbacks {
            listener(width, height)
        }
    } else {
        log.error("GLFW framebuffer size callback on unregistered window")
    }
}

@(private)
glfw_error_report :: proc(error: i32, description: cstring) {
    fmt.eprintf("GLFW: %s\n", description)
}

// A struct to wrap the GLFW window
Lambda_Window :: struct {
    handle: glfw.WindowHandle,

    width: i32,
    height: i32,

    resize_callbacks: [dynamic]proc(width: i32, height: i32),
}

// Create an instance of Lambda_Window using the provided @width, @height, and @title as GLFW params
create_lambda_window :: proc(width: i32, height: i32, title: string) -> (window: Lambda_Window) {
    // Check if glfw can initialise
    if !bool(glfw.Init()) {
        log.error("Failed to initalise GLFW")
        return
    }
    log.debug("Initialised GLFW")

    using global

    // Set window hints for OpenGL version and profile
    glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR, i32(GL_VERSION.major))
    glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR, i32(GL_VERSION.minor))
    glfw.WindowHint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

    // Set the error reporting callback for glfw
    glfw.SetErrorCallback(glfw.ErrorProc(glfw_error_report))

    // Convert provided title string to a C-style string (requires explicit deallocation)
    ctitle := strings.clone_to_cstring(title); defer delete(ctitle)

    // Finally, create the GLFW window
    window.handle = glfw.CreateWindow(width, height, ctitle, nil, nil)

    // If glfw.CreateWindow() returned nil, then something went wrong
    if window.handle == nil {
        log.error("Failed to create GLFW window")
        return
    }
    log.debug("Created GLFW window")

    // Set the program's OpenGL context to this window
    glfw.MakeContextCurrent(window.handle)

    // Set the framebuffer resize callback for this window
    lambda_windows[window.handle] = &window
    glfw.SetFramebufferSizeCallback(window.handle, glfw.FramebufferSizeProc(framebuffer_size_callback))
    glfw.SetInputMode(window.handle, glfw.CURSOR, glfw.CURSOR_DISABLED)

    // Load OpenGL up to the versions specified in the global package
    gl.load_up_to(GL_VERSION.major, GL_VERSION.minor, glfw.gl_set_proc_address)
    log.infof("Loaded OpenGL up to version %d.%d", GL_VERSION.major, GL_VERSION.minor)

    // Set the initial OpenGL viewport size
    gl.Viewport(0, 0, width, height)

    window.width = width
    window.height = height
    return
}

tick :: proc(using window: ^Lambda_Window) {
    if glfw.WindowShouldClose(handle) {
        global.SHOULD_CLOSE = true 
        return
    }

    if !global.SHOULD_CLOSE {
        glfw.SwapBuffers(handle)
        glfw.PollEvents()
    }
}

register_resize_callback :: proc(using window: ^Lambda_Window, callback: proc(width, height: i32)) {
    append(&resize_callbacks, callback)
}

destroy :: proc(using window: ^Lambda_Window) {
    log.debug("Terminating GLFW window")
    glfw.DestroyWindow(handle)
    glfw.Terminate()
}