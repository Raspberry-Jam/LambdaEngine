// TODO: Replace fmt.eprintln() calls with actual logging
// TODO: Also add logging where it ought to be
// TODO: Review names of everything, a lot of proc parameter names aren't great
// TODO: Lots and lots of doc comments

package lambda

import "core:fmt"
import glm "core:math/linalg/glsl"
import "core:log"

import "vendor:glfw"
import gl "vendor:OpenGL"

import "renderer/shader"
import "renderer/buffer"
import vdesc "renderer/shader/vertex_descriptor"
import "global"
import "window"
import "input"
import "renderer/camera"

framebuffer_size_callback :: proc(window: glfw.WindowHandle, width: i32, height: i32) {
    gl.Viewport(0, 0, width, height)
}

glfw_error_callback :: proc(error: i32, description: cstring) {
    log.errorf("GLFW: %s", description)
}

@(private)
cam_ptr: ^camera.Camera3D

main :: proc() {
    // Initialise console logger with the following format options
    log_opts := log.Options {
        .Level,
        .Time,
        .Short_File_Path,
        .Terminal_Color,
        .Procedure,
    }
    context.logger = log.create_console_logger(.Debug, log_opts, "lambda")

    l_window := window.create_lambda_window(800, 600, "Lambda Window!"); defer window.destroy(&l_window)

    cam := camera.create_perspective_camera3d(l_window.width, l_window.height, 45)
    cam_ptr = &cam

    resize_cam :: proc(width, height: i32) {
        camera.recalculate_projection_matrix(cam_ptr, width, height)
    }

    window.register_resize_callback(&l_window, resize_cam)

    input.init_key_handler(&l_window); defer input.destroy_window_key_handler(&l_window)
    input.init_mouse_handler(&l_window); defer input.destroy_window_mouse_handler(&l_window)

    input.register_key_listener(&l_window, .ESCAPE, close_on_esc_press)
    input.register_mouse_listener(&l_window, mouse_callback)

    gl.ClearColor(0.2, 0.2, 0.2, 1.0)

    Vertex :: struct {
        pos: [3]f32,
        col: [4]f32,
    }

    vertices := [] Vertex {
        {{-0.5, +0.5, 0}, {1.0, 0.0, 0.0, 0.75}},
		{{-0.5, -0.5, 0}, {1.0, 1.0, 0.0, 0.75}},
		{{+0.5, -0.5, 0}, {0.0, 1.0, 0.0, 0.75}},
		{{+0.5, +0.5, 0}, {0.0, 0.0, 1.0, 0.75}},
    }

    indices := [] u32 {
        0, 1, 2,
        2, 3, 0,
    }

    pyramid := [] Vertex {
        {{0.0, 0.5, 0.0}, {1.0, 1.0, 1.0, 1.0}}, // Top
        {{-0.5, -0.5, -0.5}, {1.0, 0.0, 0.0, 1.0}}, // Back left
        {{0.5, -0.5, -0.5}, {0.0, 1.0, 0.0, 1.0}}, // Back right
        {{-0.5, -0.5, 0.5}, {0.0, 0.0, 1.0, 1.0}}, // Front left
        {{0.5, -0.5, 0.5}, {1.0, 1.0, 0.0, 1.0}}, // Front right
    }

    pyramid_i := [] u32 {
        1, 2, 0, // Back tri
        1, 2, 3, 3, 4, 2, // Bottom square
        1, 3, 0, // Left tri
        2, 4, 0, // Right tri
        3, 4, 0, // Front tri
    }

    vertex_descriptor := vdesc.create_descriptor(); defer vdesc.delete_descriptor(&vertex_descriptor)
    vertex_descriptor \
        ->float(3) \
        ->float(4)
    
    log.debug("Created shader vertex descriptor")

    program, ok := shader.create(VERTEX_SOURCE, FRAG_SOURCE, &vertex_descriptor)
    if !ok {
        log.error("Failed to create shader program")
        return
    }
    defer shader.destroy(&program)
    log.debug("Created shader program")

    vertex_buffer := buffer.generate_vertex(.Static_Draw); defer buffer.destroy(&vertex_buffer)
    index_buffer := buffer.generate_index(.Static_Draw); defer buffer.destroy(&index_buffer)
    log.debug("Created vertex and index buffers")

    shader.use_buffer(&program, &vertex_buffer)
    log.debugf("Bound vertex buffer [%d] to shader program [%d]", vertex_buffer.handle, program.handle)
    
    buffer.bind(&vertex_buffer)
    buffer.bind(&index_buffer)
    buffer.set_data(&vertex_buffer, &pyramid)
    buffer.set_data(&index_buffer, &pyramid_i)
    log.debug("Uploaded vertices data to GPU")

    identity := cast(matrix[4, 4]f32) 1

    model_mat4 := glm.mat4Scale(glm.vec3{10, 10, 10})

    shader.bind(&program)
    shader.set_mat4x4_string(&program, "model", &model_mat4)

    for !global.SHOULD_CLOSE {
        gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
        handle_movement(&l_window)
        shader.bind(&program)
        shader.set_mat4x4_string(&program, "view", &cam.view)
        shader.set_mat4x4_string(&program, "projection", &cam.projection)
        gl.DrawElements(gl.TRIANGLES, i32(len(pyramid_i)), gl.UNSIGNED_INT, nil)
        window.tick(&l_window)
    }
    log.info("Exiting...")
}

close_on_esc_press :: proc(state: input.Key_State) {
    if state == .DOWN {
        global.SHOULD_CLOSE = true
    }
}

handle_movement :: proc(w: ^window.Lambda_Window) {
    if input.get_key(w, .A) == .DOWN {
        cam_ptr.position -= cam_ptr.right * camera.SPEED
    }
    if input.get_key(w, .D) == .DOWN {
        cam_ptr.position += cam_ptr.right * camera.SPEED
    }
    if input.get_key(w, .W) == .DOWN {
        cam_ptr.position += cam_ptr.front * camera.SPEED
    }
    if input.get_key(w, .S) == .DOWN {
        cam_ptr.position -= cam_ptr.front * camera.SPEED
    }
    if input.get_key(w, .LEFT_SHIFT) == .DOWN {
        cam_ptr.position -= cam_ptr.up * camera.SPEED
    }
    if input.get_key(w, .SPACE) == .DOWN {
        cam_ptr.position += cam_ptr.up * camera.SPEED
    }
    camera.recalculate_view_matrix(cam_ptr)
}

mouse_callback :: proc(xpos: f64, ypos: f64) {
    @(static) lastX, lastY: f64

    xoffset := f32(xpos - lastX) * camera.SENSITIVITY
    yoffset := f32(lastY - ypos) * camera.SENSITIVITY

    cam_ptr.yaw += xoffset
    cam_ptr.pitch += yoffset

    lastX = xpos
    lastY = ypos
    
    if cam_ptr.pitch > 89.0 do cam_ptr.pitch = 89.0
    if cam_ptr.pitch < -89.0 do cam_ptr.pitch = -89.0

    camera.recalculate_view_matrix(cam_ptr)
}

VERTEX_SOURCE := transmute(string) #load("shaders/vert.glsl")
FRAG_SOURCE := transmute(string) #load("shaders/frag.glsl")