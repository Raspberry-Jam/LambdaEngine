package input

import "core:log"

import "vendor:glfw"

import "lambda:window"

@(private)
key_listeners := make(map[glfw.WindowHandle] [dynamic]Key_Listener)

@(private)
forward_glfw_key_events :: proc(window: glfw.WindowHandle, key: i32, scancode: i32, action: i32, mods: i32) {
    state: Key_State = .UP if action == glfw.RELEASE else .DOWN

    if listeners, ok := key_listeners[window]; ok {
        for listener in listeners {
            if listener.key == Key(key) {
                listener.callback(state)
            }
        }
    }
}
// A set of all valid keycodes
Key :: enum {
    UNKNOW = glfw.KEY_UNKNOWN,
    SPACE = glfw.KEY_SPACE,
    APOSTROPHE = glfw.KEY_APOSTROPHE,
    COMMA = glfw.KEY_COMMA,
    MINUS = glfw.KEY_MINUS,
    PERIOD = glfw.KEY_PERIOD,
    SLASH = glfw.KEY_SLASH,
    SEMICOLON = glfw.KEY_SEMICOLON,
    EQUAL = glfw.KEY_EQUAL,
    LEFT_BRACKET = glfw.KEY_LEFT_BRACKET,
    BACKSLASH = glfw.KEY_BACKSLASH,
    RIGHT_BRACKET = glfw.KEY_RIGHT_BRACKET,
    GRAVE_ACCENT = glfw.KEY_GRAVE_ACCENT,
    WORLD_1 = glfw.KEY_WORLD_1,
    WORLD_2 = glfw.KEY_WORLD_2,
    N0 = glfw.KEY_0,
    N1 = glfw.KEY_1,
    N2 = glfw.KEY_2,
    N3 = glfw.KEY_3,
    N4 = glfw.KEY_4,
    N5 = glfw.KEY_5,
    N6 = glfw.KEY_6,
    N7 = glfw.KEY_7,
    N8 = glfw.KEY_8,
    N9 = glfw.KEY_9,
    A = glfw.KEY_A,
    B = glfw.KEY_B,
    C = glfw.KEY_C,
    D = glfw.KEY_D,
    E = glfw.KEY_E,
    F = glfw.KEY_F,
    G = glfw.KEY_G,
    H = glfw.KEY_H,
    I = glfw.KEY_I,
    J = glfw.KEY_J,
    K = glfw.KEY_K,
    L = glfw.KEY_L,
    M = glfw.KEY_M,
    N = glfw.KEY_N,
    O = glfw.KEY_O,
    P = glfw.KEY_P,
    Q = glfw.KEY_Q,
    R = glfw.KEY_R,
    S = glfw.KEY_S,
    T = glfw.KEY_T,
    U = glfw.KEY_U,
    V = glfw.KEY_V,
    W = glfw.KEY_W,
    X = glfw.KEY_X,
    Y = glfw.KEY_Y,
    Z = glfw.KEY_Z,
    ESCAPE = glfw.KEY_ESCAPE,
    ENTER = glfw.KEY_ENTER,
    TAB = glfw.KEY_TAB,
    BACKSPACE = glfw.KEY_BACKSPACE,
    INSERT = glfw.KEY_INSERT,
    DELETE = glfw.KEY_DELETE,
    RIGHT = glfw.KEY_RIGHT,
    LEFT = glfw.KEY_LEFT,
    DOWN = glfw.KEY_DOWN,
    UP = glfw.KEY_UP,
    PAGE_UP = glfw.KEY_PAGE_UP,
    PAGE_DOWN = glfw.KEY_PAGE_DOWN,
    HOME = glfw.KEY_HOME,
    END = glfw.KEY_END,
    CAPS_LOCK = glfw.KEY_CAPS_LOCK,
    SCROLL_LOCK = glfw.KEY_SCROLL_LOCK,
    NUM_LOCK = glfw.KEY_NUM_LOCK,
    PRINT_SCREEN = glfw.KEY_PRINT_SCREEN,
    PAUSE = glfw.KEY_PAUSE,
    F1 = glfw.KEY_F1,
    F2 = glfw.KEY_F2,
    F3 = glfw.KEY_F3,
    F4 = glfw.KEY_F4,
    F5 = glfw.KEY_F5,
    F6 = glfw.KEY_F6,
    F7 = glfw.KEY_F7,
    F8 = glfw.KEY_F8,
    F9 = glfw.KEY_F9,
    F10 = glfw.KEY_F10,
    F11 = glfw.KEY_F11,
    F12 = glfw.KEY_F12,
    F13 = glfw.KEY_F13,
    F14 = glfw.KEY_F14,
    F15 = glfw.KEY_F15,
    F16 = glfw.KEY_F16,
    F17 = glfw.KEY_F17,
    F18 = glfw.KEY_F18,
    F19 = glfw.KEY_F19,
    F20 = glfw.KEY_F20,
    F21 = glfw.KEY_F21,
    F22 = glfw.KEY_F22,
    F23 = glfw.KEY_F23,
    F24 = glfw.KEY_F24,
    F25 = glfw.KEY_F25,
    KP_0 = glfw.KEY_KP_0,
    KP_1 = glfw.KEY_KP_1,
    KP_2 = glfw.KEY_KP_2,
    KP_3 = glfw.KEY_KP_3,
    KP_4 = glfw.KEY_KP_4,
    KP_5 = glfw.KEY_KP_5,
    KP_6 = glfw.KEY_KP_6,
    KP_7 = glfw.KEY_KP_7,
    KP_8 = glfw.KEY_KP_8,
    KP_9 = glfw.KEY_KP_9,
    KP_DECIMAL = glfw.KEY_KP_DECIMAL,
    KP_DIVIDE = glfw.KEY_KP_DIVIDE,
    KP_MULTIPLY = glfw.KEY_KP_MULTIPLY,
    KP_SUBTRACT = glfw.KEY_KP_SUBTRACT,
    KP_ADD = glfw.KEY_KP_ADD,
    KP_ENTER = glfw.KEY_KP_ENTER,
    KP_EQUAL = glfw.KEY_KP_EQUAL,
    LEFT_SHIFT = glfw.KEY_LEFT_SHIFT,
    LEFT_CONTROL = glfw.KEY_LEFT_CONTROL,
    LEFT_ALT = glfw.KEY_LEFT_ALT,
    LEFT_SUPER = glfw.KEY_LEFT_SUPER,
    RIGHT_SHIFT = glfw.KEY_RIGHT_SHIFT,
    RIGHT_CONTROL = glfw.KEY_RIGHT_CONTROL,
    RIGHT_ALT = glfw.KEY_RIGHT_ALT,
    RIGHT_SUPER = glfw.KEY_RIGHT_SUPER,
    MENU = glfw.KEY_MENU,
}

// Possible states for a key to be in
Key_State :: enum { UP, DOWN }

@(private)
Key_Listener :: struct {
    key: Key,
    callback: proc(state: Key_State),
}

// Initialise a key handler for the provided window
init_key_handler :: proc(window: ^window.Lambda_Window) {
    glfw.SetKeyCallback(window.handle, glfw.KeyProc(forward_glfw_key_events))
}

// Register a listener function to a specific key for the provided window's events
register_key_listener :: proc(window: ^window.Lambda_Window, key: Key, callback: proc(state: Key_State)) {
    // Check if the key exists, and if not, instantiate a dynamic array with the new key
    if listeners, ok := key_listeners[window.handle]; !ok {
        key_listeners[window.handle] = make([dynamic]Key_Listener, 0, 1)
    }

    append(&key_listeners[window.handle], Key_Listener{key, callback})
}

// Get the state of a key
get_key :: proc(window: ^window.Lambda_Window, key: Key) -> Key_State {
    glfw_state := glfw.GetKey(window.handle, i32(key))
    return .UP if glfw_state == glfw.RELEASE else .DOWN
}

// Clean up the key handler attached to this window
destroy_window_key_handler :: proc(window: ^window.Lambda_Window) {
    delete(key_listeners[window.handle])
    delete_key(&key_listeners, window.handle)
    log.debug("Cleaning up keyboard handler for window", uintptr(window.handle))
}

// Clean up all key handlers for all windows
destroy_all_key_handlers :: proc() {
    for k, v in key_listeners {
        delete(v)
    }
    delete(key_listeners)
    log.debug("Cleaning up all keyboard handlers")
}
