package input

import "core:log"

import "vendor:glfw"

import "lambda:window"

@(private)
mouse_listeners := make(map[glfw.WindowHandle] [dynamic]proc(xpos: f64, ypos: f64))

@(private)
forward_glfw_mouse_events :: proc(window: glfw.WindowHandle, xpos, ypos: f64) {
    if listener_arr, ok := mouse_listeners[window]; ok {
        for listener in listener_arr {
            listener(xpos, ypos)
        }
    }
}

// Initialise a mouse handler for the provided window
init_mouse_handler :: proc(window: ^window.Lambda_Window) {
    glfw.SetCursorPosCallback(window.handle, glfw.CursorPosProc(forward_glfw_mouse_events))
}

// Register a listener function to the current window for all mouse position updates
register_mouse_listener :: proc(window: ^window.Lambda_Window, callback: proc(xpos: f64, ypos: f64)) {
    // Check if the key exists, and if not, instantiate a dynamic array with the new key
    if l, ok := mouse_listeners[window.handle]; !ok {
        mouse_listeners[window.handle] = make([dynamic]proc(xpos: f64, ypos: f64), 0, 1)
    }
    append(&mouse_listeners[window.handle], callback)
}

// Clean up the mouse handler attached to this window
destroy_window_mouse_handler :: proc(window: ^window.Lambda_Window) {
    delete(mouse_listeners[window.handle])
    delete_key(&mouse_listeners, window.handle)
    log.debugf("Cleaning up mouse handler for window %d", uintptr(window.handle))
}

// Clean up all mouse handlers for all windows
destroy_all_mouse_handlers :: proc() {
    for k, v in mouse_listeners {
        delete(v)
    }
    delete(mouse_listeners)
    log.debug("Cleaning up all mouse handlers")
}