package buffer

import "core:fmt"

import gl "vendor:OpenGL"
import global "../../global"

// How this buffer will be stored in VRAM for optimal access.
Memory_Mode :: enum u32 {
    Static_Draw = gl.STATIC_DRAW,
    Static_Read = gl.STATIC_READ,
    Static_Copy = gl.STATIC_COPY,
    Dynamic_Draw = gl.DYNAMIC_DRAW,
    Dynamic_Read = gl.DYNAMIC_READ,
    Dynamic_Copy = gl.DYNAMIC_COPY,
    Stream_Draw = gl.STREAM_DRAW,
    Stream_Read = gl.STREAM_READ,
    Stream_Copy = gl.STREAM_COPY,
}

@(private)
Abstract_Buffer :: struct {
    handle: u32,
    memory_mode: Memory_Mode,
    length: int,
    _gl_type: u32,
}

// A CPU interface to a vertex buffer on the GPU. 
Vertex_Buffer :: struct {
    using abstract_buffer: Abstract_Buffer,
}

// A CPU interface to an index buffer on the GPU.
Index_Buffer :: struct {
    using abstract_buffer: Abstract_Buffer,
}

// Construct a Vertex_Buffer object.
// Allocates a buffer on GPU memory, using specified memory mode.
generate_vertex :: proc(memory_mode: Memory_Mode) -> Vertex_Buffer {
    abs := Abstract_Buffer{}
    gl.GenBuffers(1, &abs.handle)
    abs.memory_mode = memory_mode
    abs._gl_type = gl.ARRAY_BUFFER
    return Vertex_Buffer {abstract_buffer = abs}
}

// Construct an Index_Buffer object.
// Allocates a buffer on GPU memory, using specified memory mode.
generate_index :: proc(memory_mode: Memory_Mode) -> Index_Buffer {
    abs := Abstract_Buffer{}
    gl.GenBuffers(1, &abs.handle)
    abs.memory_mode = memory_mode
    abs._gl_type = gl.ELEMENT_ARRAY_BUFFER
    return Index_Buffer{abstract_buffer = abs}
}

// Bind this buffer for usage with subsequent rendering commands.
bind :: proc(using buffer: ^Abstract_Buffer) {
    gl.BindBuffer(_gl_type, handle)
}

@(private)
_reallocate_buffer :: proc(using buffer: ^Abstract_Buffer, size: int){
    length = size // Update internal buffer length value
    bind(buffer)
    gl.BufferData(_gl_type, length, rawptr(uintptr(0)), u32(memory_mode))
}

@(private)
_set_buffer_data :: proc(using buffer: ^Abstract_Buffer, size: int, data: ^$T, offset: int){
    if size + offset > length {
        _reallocate_buffer(buffer, size)
    }
    if global.GL_VERSION.major >= 4 && global.GL_VERSION.minor >= 5 {
        gl.NamedBufferSubData(handle, offset, size, raw_data(data^))
    } else {
        bind(buffer)
        gl.BufferSubData(_gl_type, offset, size, raw_data(data^))
    }
}

// Upload data from the CPU to this buffer in GPU memory.
set_vertex_buffer_data :: proc(using buffer: ^Vertex_Buffer, data: ^[]$Vertex, offset := 0) {
    _set_buffer_data(buffer, len(data^) * size_of(data^[0]), data, offset)
}

// Upload data from the CPU to this buffer in GPU memory.
set_index_buffer_data :: proc(using buffer: ^Index_Buffer, data: ^[]u32, offset := 0) {
    _set_buffer_data(buffer, len(data^) * size_of(data^[0]), data, offset)
}

// Upload data from the CPU to this buffer in GPU memory.
set_data :: proc {
    set_vertex_buffer_data,
    set_index_buffer_data,
}

// Deallocate this buffer from GPU memory.
destroy :: proc(using buffer: ^Abstract_Buffer) {
    gl.DeleteBuffers(1, &handle)
}