package vertex_descriptor

import gl "vendor:OpenGL"

// The type of this vertex attribute, and the number of components in this attribute
Vertex_Attribute :: struct {
    type: u32,
    count: i32,
}

// A builder struct to easily create a list of vertex attributes to be consumed by a vertex array.
Vertex_Descriptor :: struct {
    attributes: [dynamic]Vertex_Attribute,
    stride: i32,

    byte:           proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
    unsigned_byte:  proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
    short:          proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
    unsigned_short: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
    int:            proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
    unsigned_int:   proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
    float:          proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
    double:         proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor,
}

// Construct the default Vertex_Descriptor, setting up its vtable
create_descriptor :: proc() -> Vertex_Descriptor {
    return Vertex_Descriptor {
        byte = vtable_byte_VDB,
        unsigned_byte = vtable_unsigned_byte_VDB,
        short = vtable_short_VDB,
        unsigned_short = vtable_unsigned_short_VDB,
        int = vtable_int_VDB,
        unsigned_int = vtable_unsigned_int_VDB,
        float = vtable_float_VDB,
        double = vtable_double_VDB,
    }
}

// Deallocate the dynamic array used to store the list of attributes
delete_descriptor :: proc(using descriptor: ^Vertex_Descriptor) {
    delete(attributes)
}

// Get the number of bytes in a gl type. (Eg: gl.FLOAT == 4 bytes)
gl_size_of :: proc(type: u32) -> i32 {
    if type == gl.BYTE || type == gl.UNSIGNED_BYTE {
        return 1
    } else if type == gl.SHORT || type == gl.UNSIGNED_SHORT {
        return 2
    } else if type == gl.INT || type == gl.UNSIGNED_INT || type == gl.FLOAT {
        return 4
    } else if type == gl.DOUBLE {
        return 8
    } else {
        return 0
    }
}

@(private)
vtable_byte_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.BYTE, count })
    stride += 1 * count
    return builder
}

@(private)
vtable_unsigned_byte_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.UNSIGNED_BYTE, count })
    stride += 1 * count
    return builder
}

@(private)
vtable_short_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.SHORT, count })
    stride += 2 * count
    return builder
}

@(private)
vtable_unsigned_short_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.UNSIGNED_SHORT, count })
    stride += 2 * count
    return builder
}

@(private)
vtable_int_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.INT, count })
    stride += 4 * count
    return builder
}

@(private)
vtable_unsigned_int_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.UNSIGNED_INT, count })
    stride += 4 * count
    return builder
}

@(private)
vtable_float_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.FLOAT, count })
    stride += 4 * count
    return builder
}

@(private)
vtable_double_VDB :: proc(using builder: ^Vertex_Descriptor, count: i32) -> ^Vertex_Descriptor {
    append(&attributes, Vertex_Attribute{ gl.DOUBLE, count })
    stride += 8 * count
    return builder
}

