//+private
package shader

import gl "vendor:OpenGL"
import vdesc "vertex_descriptor"

// A basic wrapper around a Vertex Array Object
Vertex_Array :: struct {
    handle: u32,
    descriptor: ^vdesc.Vertex_Descriptor,
}

// Allocate a Vertex Array on the GPU, and store a pointer to the Vertex Descriptor
// that will be used when a Vertex Buffer is bound to the shader this VAO belongs to
create_vertex_array :: proc(descriptor: ^vdesc.Vertex_Descriptor) -> (vao: Vertex_Array) {
    gl.GenVertexArrays(1, &vao.handle)
    vao.descriptor = descriptor
    return
}

// Bind this VAO
bind_vertex_array :: proc(using vao: ^Vertex_Array) {
    gl.BindVertexArray(handle)
}

// Deallocate this VAO in GPU memory
delete_vertex_array :: proc(using vao: ^Vertex_Array) {
    gl.DeleteVertexArrays(1, &handle)
}