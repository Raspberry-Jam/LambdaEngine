package shader

import gl "vendor:OpenGL"
import "core:fmt"
import "core:log"

import "lambda:renderer/buffer"
import vdesc "lambda:renderer/shader/vertex_descriptor"

// A data structure to keep track of and represent an OpenGL shader program
// NOTE: This is subject to change in the future, with the integration of other graphics APIs such as Vulkan
Shader :: struct {
    handle: u32,
    _uniform_cache: gl.Uniforms,
    vertex_array: Vertex_Array,
}

// Create a Shader instance, compiling the complete OpenGL shader program. 
// Returns a boolean for its status, true for successful, false for failure.
create :: proc(vertex_source: string, fragment_source: string, vertex_descriptor: ^vdesc.Vertex_Descriptor) -> (shader: Shader, ok: bool) {
    shader.handle, ok = gl.load_shaders_source(vertex_source, fragment_source) // Compiles, links, and cleans up shader modules

    // If a failure occured, return a default struct and indicate an error occurred
    if !ok {
        log.warn("Failed to compile shader program")
        return Shader{}, false
    }

    log.debug("Successfully compiled shader program from source")

    // Populate the uniforms cache
    shader._uniform_cache = gl.get_uniforms_from_program(shader.handle)
    log.debug("Populated CPU shader uniform cache")

    // Create the Vertex Array
    shader.vertex_array = create_vertex_array(vertex_descriptor)
    log.debug("Generated shader's VAO")
    return
}

// Bind this shader to be used with the subsequent GPU operations.
// NOTE: In future may have error checking if the pipeline cannot bind this shader for some reason
bind :: proc(using program: ^Shader) {
    gl.UseProgram(handle)
    bind_vertex_array(&vertex_array)
}

// Query the shader program to find the layout location of given attribute.
get_attrib_location :: proc(using program: ^Shader, attrib_name: string) -> u32 {
    return u32(gl.GetAttribLocation(handle, cstring(raw_data(attrib_name))))
}

// Query the uniform cache of the shader program to find the location of the given uniform.
get_uniform_location :: proc(using program: ^Shader, uniform_name: string) -> i32 {
    return _uniform_cache[uniform_name].location
}

// Deallocate all memory associated with the shader object on the CPU, and mark the program for deletion on the GPU.
destroy :: proc(using program: ^Shader) {
    gl.destroy_uniforms(_uniform_cache)
    gl.DeleteProgram(handle)
    delete_vertex_array(&vertex_array)
    log.debugf("Destroyed shader [ID: %d]", handle)
}

// Bind the shader's vertex array object to the given vertex buffer.
use_buffer :: proc(using shader: ^Shader, vbo: ^buffer.Vertex_Buffer) {
    bind_vertex_array(&vertex_array)
    buffer.bind(vbo)
    log.debug("Bound vertex buffer to shader's VAO")

    offset: uintptr = 0

    for i in 0..<len(vertex_array.descriptor.attributes) {
        attr := vertex_array.descriptor.attributes[i]
        gl.EnableVertexAttribArray(u32(i))
        gl.VertexAttribPointer(u32(i), attr.count, attr.type, false, vertex_array.descriptor.stride, offset)
        offset += uintptr(attr.count * vdesc.gl_size_of(attr.type))
    }
    log.debug("Set vertex attrib pointers")
}

// Set the int at @uniform_loc to i32 @value
set_int :: proc(uniform_loc: i32, value: $T/i32) {
    gl.Uniform1i(uniform_loc, i32(value))
}

// Wrapper for set_int() that automatically retrieves uniform location from shader cache
set_int_string :: proc(using program: ^Shader, uniform_name: string, value: $T/i32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_int(uniform_loc, value)
}

// Set the uint at @uniform_loc to u32 @value
set_uint :: proc(uniform_loc: i32, value: $T/u32) {
    gl.Uniform1ui(uniform_loc, u32(value))
}

// Wrapper for set_uint() that automatically retrieves uniform location from shader cache
set_uint_string :: proc(using program: ^Shader, uniform_name: string, value: $T/u32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_uint(uniform_loc, value)
}

// Set the float at @uniform_loc to f32 @value
set_float :: proc(uniform_loc: i32, value: $T/f32) {
    gl.Uniform1f(uniform_loc, f32(value))
}

// Wrapper for set_float() that automatically retrieves uniform location from shader cache
set_float_string :: proc(using program: ^Shader, uniform_name: string, value: $T/f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_float(uniform_loc, value)
}

// Set the double at @uniform_loc to f64 @value
set_double :: proc(uniform_loc: i32, value: $T/f64) {
    gl.Uniform1d(uniform_loc, f64(value))
}

// Wrapper for set_double() that automatically retrieves uniform location from shader cache
set_double_string :: proc(using program: ^Shader, uniform_name: string, value: $T/f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_double(uniform_loc, value)
}

// Set the texture slot for @sampler to value in @slot
set_texture :: proc(sampler: i32, slot: $T/i32) {
    set_int(sampler, slot)
}

// Wrapper for set_texture() that automatically retrieves uniform location from shader cache
set_texture_string :: proc(using program: ^Shader, sampname: string, slot: $T/i32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_texture(uniform_loc, value)
}

// Set the vec2f at @uniform_loc to [2]f32 @value
set_vec2f :: proc(uniform_loc: i32, value: ^$T/[2]f32) {
    gl.Uniform2fv(uniform_loc, 1, cast([^]f32)&value[0])
}

// Wrapper for set_vec2f() that automatically retrieves uniform location from shader cache
set_vec2f_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[2]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec2f(uniform_loc, value)
}

// Set the vec3f at @uniform_loc to [3]f32 @value
set_vec3f :: proc(uniform_loc: i32, value: ^$T/[3]f32) {
    gl.Uniform3fv(uniform_loc, 1, cast([^]f32)&value[0])
}

// Wrapper for set_vec3f() that automatically retrieves uniform location from shader cache
set_vec3f_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[3]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec3f(uniform_loc, value)
}

// Set the vec4f at @uniform_loc to [4]f32 @value
set_vec4f :: proc(uniform_loc: i32, value: ^$T/[4]f32) {
    gl.Uniform4fv(uniform_loc, 1, cast([^]f32)&value[0])
}

// Wrapper for set_vec4f() that automatically retrieves uniform location from shader cache
set_vec4f_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[4]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec4f(uniform_loc, value)
}

// Set the vec2d at @uniform_loc to [2]f64 @value
set_vec2d :: proc(uniform_loc: i32, value: ^$T/[2]f64) {
    gl.Uniform2dv(uniform_loc, 1, cast([^]f64)&value[0])
}

// Wrapper for set_vec2d() that automatically retrieves uniform location from shader cache
set_vec2d_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[2]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec2d(uniform_loc, value)
}

// Set the vec3d at @uniform_loc to [3]f64 @value
set_vec3d :: proc(uniform_loc: i32, value: ^$T/[3]f64) {
    gl.Uniform3dv(uniform_loc, 1, cast([^]f64)&value[0])
}

// Wrapper for set_vec3d() that automatically retrieves uniform location from shader cache
set_vec3d_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[3]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec3d(uniform_loc, value)
}

// Set the vec4d at @uniform_loc to [4]f64 @value
set_vec4d :: proc(uniform_loc: i32, value: ^$T/[4]f64) {
    gl.Uniform4dv(uniform_loc, 1, cast([^]f64)&value[0])
}

// Wrapper for set_vec4d() that automatically retrieves uniform location from shader cache
set_vec4d_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[4]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec4d(uniform_loc, value)
}

// Set the vec2i at @uniform_loc to [2]i32 @value
set_vec2i :: proc(uniform_loc: i32, value: ^$T/[2]i32) {
    gl.Uniform2iv(uniform_loc, 1, cast([^]i32)&value[0])
}

// Wrapper for set_vec2i() that automatically retrieves uniform location from shader cache
set_vec2i_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[2]i32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec2i(uniform_loc, value)
}

// Set the vec3i at @uniform_loc to [3]i32 @value
set_vec3i :: proc(uniform_loc: i32, value: ^$T/[3]i32) {
    gl.Uniform3iv(uniform_loc, 1, cast([^]i32)&value[0])
}

// Wrapper for set_vec3i() that automatically retrieves uniform location from shader cache
set_vec3i_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[3]i32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec3i(uniform_loc, value)
}

// Set the vec4i at @uniform_loc to [4]i32 @value
set_vec4i :: proc(uniform_loc: i32, value: ^$T/[4]i32) {
    gl.Uniform4iv(uniform_loc, 1, cast([^]i32)&value[0])
}

// Wrapper for set_vec4i() that automatically retrieves uniform location from shader cache
set_vec4i_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[4]i32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec4i(uniform_loc, value)
}

// Set the vec2u at @uniform_loc to [2]u32 @value
set_vec2u :: proc(uniform_loc: i32, value: ^$T/[2]u32) {
    gl.Uniform2uiv(uniform_loc, 1, cast([^]u32)&value[0])
}

// Wrapper for set_vec2u() that automatically retrieves uniform location from shader cache
set_vec2u_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[2]u32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec2u(uniform_loc, value)
}

// Set the vec3u at @uniform_loc to [3]u32 @value
set_vec3u :: proc(uniform_loc: i32, value: ^$T/[3]u32) {
    gl.Uniform3uiv(uniform_loc, 1, cast([^]u32)&value[0])
}

// Wrapper for set_vec3u() that automatically retrieves uniform location from shader cache
set_vec3u_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[3]u32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec3u(uniform_loc, value)
}

// Set the vec4u at @uniform_loc to [4]u32 @value
set_vec4u :: proc(uniform_loc: i32, value: ^$T/[4]u32) {
    gl.Uniform4uiv(uniform_loc, 1, cast([^]u32)&value[0])
}

// Wrapper for set_vec4u() that automatically retrieves uniform location from shader cache
set_vec4u_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/[4]u32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_vec4u(uniform_loc, value)
}

// Set the mat2 at @uniform_loc to matrix[2, 2]f32 @value
set_mat2x2 :: proc(uniform_loc: i32, value: ^$T/matrix[2, 2]f32) {
    gl.UniformMatrix2fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat2x2() that automatically retrieves uniform location from shader cache
set_mat2x2_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[2, 2]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat2x2(uniform_loc, value)
}

// Set the mat2x3 at @uniform_loc to matrix[2, 3]f32 @value
set_mat2x3 :: proc(uniform_loc: i32, value: ^$T/matrix[2, 3]f32) {
    gl.UniformMatrix2x3fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat2x3() that automatically retrieves uniform location from shader cache
set_mat2x3_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[2, 3]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat2x3(uniform_loc, value)
}

// Set the mat2x4 at @uniform_loc to matrix[2, 4]f32 @value
set_mat2x4 :: proc(uniform_loc: i32, value: ^$T/matrix[2, 4]f32) {
    gl.UniformMatrix2x4fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat2x4() that automatically retrieves uniform location from shader cache
set_mat2x4_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[2, 4]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat2x4(uniform_loc, value)
}

// Set the mat3x2 at @uniform_loc to matrix[3, 2]f32 @value
set_mat3x2 :: proc(uniform_loc: i32, value: ^$T/matrix[3, 2]f32) {
    gl.UniformMatrix3x2fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat3x2() that automatically retrieves uniform location from shader cache
set_mat3x2_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[3, 2]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat3x2(uniform_loc, value)
}

// Set the mat3 at @uniform_loc to matrix[3, 3]f32 @value
set_mat3x3 :: proc(uniform_loc: i32, value: ^$T/matrix[3, 3]f32) {
    gl.UniformMatrix3fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat3x3() that automatically retrieves uniform location from shader cache
set_mat3x3_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[3, 3]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat3x3(uniform_loc, value)
}

// Set the mat3x4 at @uniform_loc to matrix[3, 4]f32 @value
set_mat3x4 :: proc(uniform_loc: i32, value: ^$T/matrix[3, 4]f32) {
    gl.UniformMatrix3x4fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat3x4() that automatically retrieves uniform location from shader cache
set_mat3x4_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[3, 4]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat3x4(uniform_loc, value)
}

// Set the mat4x2 at @uniform_loc to matrix[4, 2]f32 @value
set_mat4x2 :: proc(uniform_loc: i32, value: ^$T/matrix[4, 2]f32) {
    gl.UniformMatrix4x2fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat4x2() that automatically retrieves uniform location from shader cache
set_mat4x2_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[4, 2]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat4x2(uniform_loc, value)
}

// Set the mat4x3 at @uniform_loc to matrix[4, 3]f32 @value
set_mat4x3 :: proc(uniform_loc: i32, value: ^$T/matrix[4, 3]f32) {
    gl.UniformMatrix4x3fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat4x3() that automatically retrieves uniform location from shader cache
set_mat4x3_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[4, 3]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat4x3(uniform_loc, value)
}

// Set the mat4 at @uniform_loc to matrix[4, 4]f32 @value
set_mat4x4 :: proc(uniform_loc: i32, value: ^$T/matrix[4, 4]f32) {
    gl.UniformMatrix4fv(uniform_loc, 1, false, cast([^]f32)&value[0, 0])
}

// Wrapper for set_mat4x4() that automatically retrieves uniform location from shader cache
set_mat4x4_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[4, 4]f32) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_mat4x4(uniform_loc, value)
}

// Set the dmat2 at @uniform_loc to matrix[2, 2]f64 @value
set_dmat2x2 :: proc(uniform_loc: i32, value: ^$T/matrix[2, 2]f64) {
    gl.UniformMatrix2dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat2x2() that automatically retrieves uniform location from shader cache
set_dmat2x2_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[2, 2]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat2x2(uniform_loc, value)
}

// Set the dmat2x3 at @uniform_loc to matrix[2, 3]f64 @value
set_dmat2x3 :: proc(uniform_loc: i32, value: ^$T/matrix[2, 3]f64) {
    gl.UniformMatrix2x3dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat2x3() that automatically retrieves uniform location from shader cache
set_dmat2x3_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[2, 3]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat2x3(uniform_loc, value)
}

// Set the dmat2x4 at @uniform_loc to matrix[2, 4]f64 @value
set_dmat2x4 :: proc(uniform_loc: i32, value: ^$T/matrix[2, 4]f64) {
    gl.UniformMatrix2x4dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat2x4() that automatically retrieves uniform location from shader cache
set_dmat2x4_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[2, 4]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat2x4(uniform_loc, value)
}

// Set the dmat3x2 at @uniform_loc to matrix[3, 2]f64 @value
set_dmat3x2 :: proc(uniform_loc: i32, value: ^$T/matrix[3, 2]f64) {
    gl.UniformMatrix3x2dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat3x2() that automatically retrieves uniform location from shader cache
set_dmat3x2_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[3, 2]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat3x2(uniform_loc, value)
}

// Set the dmat3 at @uniform_loc to matrix[3, 3]f64 @value
set_dmat3x3 :: proc(uniform_loc: i32, value: ^$T/matrix[3, 3]f64) {
    gl.UniformMatrix3dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat3x3() that automatically retrieves uniform location from shader cache
set_dmat3x3_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[3, 3]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat3x3(uniform_loc, value)
}

// Set the dmat3x4 at @uniform_loc to matrix[3, 4]f64 @value
set_dmat3x4 :: proc(uniform_loc: i32, value: ^$T/matrix[3, 4]f64) {
    gl.UniformMatrix3x4dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat3x4() that automatically retrieves uniform location from shader cache
set_dmat3x4_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[3, 4]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat3x4(uniform_loc, value)
}

// Set the dmat4x2 at @uniform_loc to matrix[4, 2]f64 @value
set_dmat4x2 :: proc(uniform_loc: i32, value: ^$T/matrix[4, 2]f64) {
    gl.UniformMatrix4x2dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat4x2() that automatically retrieves uniform location from shader cache
set_dmat4x2_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[4, 2]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat4x2(uniform_loc, value)
}

// Set the dmat4x3 at @uniform_loc to matrix[4, 3]f64 @value
set_dmat4x3 :: proc(uniform_loc: i32, value: ^$T/matrix[4, 3]f64) {
    gl.UniformMatrix4x3dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat4x3() that automatically retrieves uniform location from shader cache
set_dmat4x3_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[4, 3]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat4x3(uniform_loc, value)
}

// Set the dmat4 at @uniform_loc to matrix[4, 4]f64 @value
set_dmat4x4 :: proc(uniform_loc: i32, value: ^$T/matrix[4, 4]f64) {
    gl.UniformMatrix4dv(uniform_loc, 1, false, cast([^]f64)&value[0, 0])
}

// Wrapper for set_dmat4x4() that automatically retrieves uniform location from shader cache
set_dmat4x4_string :: proc(using program: ^Shader, uniform_name: string, value: ^$T/matrix[4, 4]f64) {
    uniform_loc := get_uniform_location(program, uniform_name)
    set_dmat4x4(uniform_loc, value)
}
