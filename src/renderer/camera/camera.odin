package camera

import "core:log"
import glm "core:math/linalg/glsl"
import "core:math/linalg"
import "core:math"

import gl "vendor:OpenGL"

SPEED :: 0.5
SENSITIVITY :: 0.1

// A data structure to keep track of the position, rotation, and relative vectors of a camera object within world space.
Camera3D :: struct {
    ortho: bool,
    projection: matrix[4, 4]f32,
    view: matrix[4, 4]f32,
    position: [3]f32,
    pitch: f32,
    yaw: f32,
    fovy: f32,
    front: [3]f32,
    up: [3]f32,
    right: [3]f32,
}

// Create a perspective 3d camera, represented by Camera3D
create_perspective_camera3d :: proc(width: i32, height: i32, fovy: f32) -> (camera: Camera3D) {
    camera.fovy = fovy
    camera.ortho = false
    recalculate_projection_matrix(&camera, width, height)
    recalculate_view_matrix(&camera)
    return
}

// Recalculate the projection matrix, also known as the view frustum
recalculate_projection_matrix :: proc(using camera: ^Camera3D, width: i32, height: i32) {
    if ortho {
        camera.projection = cast(matrix[4, 4]f32) glm.mat4Ortho3d(0, 0, f32(width), f32(height), -1.0, 1.0)
    } else {
        aspect := f32(width) / f32(height)
        camera.projection = cast(matrix[4, 4]f32) glm.mat4Perspective(glm.radians(fovy), aspect, 0.1, 100.0)
    }
}

// Recalculate the view matrix with current camera position values
recalculate_view_matrix :: proc(using camera: ^Camera3D) {
    // Get relative position vectors
    using math
    front = [3]f32 {
        cos(to_radians(yaw)) * cos(to_radians(pitch)),
        sin(to_radians(pitch)),
        sin(to_radians(yaw)) * cos(to_radians(pitch)),
    }
    n_front := glm.normalize_vec3(cast(glm.vec3)front)
    
    WORLD_UP :: glm.vec3{0.0, 1.0, 0.0}

    right = cast([3]f32) glm.normalize_vec3(glm.cross_vec3(n_front, WORLD_UP))
    up = cast([3]f32) glm.normalize_vec3(glm.cross(glm.vec3(right), n_front))

    view = cast(matrix[4, 4]f32) glm.mat4LookAt(cast(glm.vec3)position, cast(glm.vec3)position + n_front, cast(glm.vec3) up)
}