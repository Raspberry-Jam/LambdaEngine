package global

WIDTH :: 800
HEIGHT :: 600
TITLE :: "WindowTest"

SHOULD_CLOSE := false

GL_VERSION :: GL_Version {
    major = 4,
    minor = 5,
}

GL_Version :: struct {
    major: int,
    minor: int,
}