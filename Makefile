SRC_DIR := src
BUILD_DIR := build

BIN_NAME := LambdaEngine

COLLECTION_NAME := lambda

build-release: $(SRC_DIR)/*.odin
	mkdir -p $(BUILD_DIR)/release
	odin build $(SRC_DIR) -out:$(BUILD_DIR)/release/$(BIN_NAME) -collection:$(COLLECTION_NAME)=$(SRC_DIR) -o:speed

build-debug: $(SRC_DIR)/*.odin
	mkdir -p $(BUILD_DIR)/debug
	odin build $(SRC_DIR) -out:$(BUILD_DIR)/debug/$(BIN_NAME) -collection:$(COLLECTION_NAME)=$(SRC_DIR) -debug